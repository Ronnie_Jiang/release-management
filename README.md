# 发行经理

Release Manager的主要工作职责是在社区协调各SIG的Maintainer、QA等各个团队，完成openEuler社区版本的发布工作。主要职责如下：

- 规划和计划openEuler版本的发行时间表
- 在开发/测试周期中跟踪（更新updates或功能feature）的开发状态
- 发布协调（在QA，发布工程师，技术委员会等范围内），参与相关组和发布相关等会议
- 负责openEuler项目的交付过程协调



## 组织会议

公开的视频会议时间：北京时间双周三上午，9:30~11:30，请订阅release@openeuler.org，以获取相关会议通知

## 会议纪要归档
release management sig历次会议纪要均做归档，详情请查询历史[会议纪要归档wiki](https://gitee.com/openeuler/release-management/wikis)

## 成员
**Maintainer：**
- 姜振华 [@Ronnie_Jiang](https://gitee.com/Ronnie_Jiang)
- 江裕民[@yuming_jiang](https://gitee.com/yuming_jiang)
- 胡峰 [@solarhu](https://gitee.com/solarhu)
- 朱延朋 [@zyp-rock](https://gitee.com/zyp-rock)
- 陈亚强 [@chenyaqiang](https://gitee.com/chenyaqiang)

**扩展成员：**
- 刘博 [@boliurise](https://gitee.com/boliurise)
- 明沛 [@dolphin-m](https://gitee.com/dolphin-m)
- 张伟 [@hwzw](https://gitee.com/hwzw)
- 李永强 [@Charlie_li](https://gitee.com/Charlie_li)

## 联系方式

邮箱列表：release@openeuler.org



## 项目清单

### Release Manager

Repository地址：https://gitee.com/openeuler/release-management

*发布经理团队的相关信息均在以上Reository内，可以进入该Repository了解详情
