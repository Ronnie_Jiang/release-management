# Release plan
|Stange name|Begin time|End time|
|:----------|:---------|:-------|
|Kernel update 5.10|2020-10-20|2020-10-30|
|Key feature collect|2020-10-10|2020-12-15|
|Kernel freezing|2020-12-21|2020-12-30|
|Key feature freezing|2021-1-22|2021-1-22|
|Build beta branch|2021-2-1|2022-2-22|
|Beta test round 1|2021-2-24|2021-3-2|
|Beta test round 2|2021-3-5|2021-3-11|
|Beta test round 3|2021-3-15|2021-5-19|
|Beta test round 4|2021-3-22|2021-3-24|
|release|2021-3-25|2021-3-26|

# Feture list
|no|fetur|status|sig|owner|
|:----|:---|:---|:--|:----|
|1|[openEuler 21.03 support openStack](https://gitee.com/openeuler/release-management/issues/I25Y6B?from=project-issue)|discussion|sig-openstack|[@joec88](https://gitee.com/joec88) [@liksh](https://gitee.com/liksh) |
|2|[openEuler 21.03 support virtualization live migration pro](https://gitee.com/openeuler/release-management/issues/I25ZB1?from=project-issue)|discussion|sig-virt|[@alexchen](https://gitee.com/alexchen)|
|3|[openEuler 21.03 support StratoVirt function enhancement](https://gitee.com/openeuler/release-management/issues/I25ZH0?from=project-issue)|discussion|sig-virt|[@alexchen](https://gitee.com/alexchen)|
|4|[openEuler 21.03 support Risc-v virt live migration](https://gitee.com/openeuler/release-management/issues/I25ZF1?from=project-issue)|discussion|sig-virt|[@alexchen](https://gitee.com/alexchen)|
|5|[openEuler 21.03 support DDE](https://gitee.com/openeuler/release-management/issues/I27TT4?from=project-issue)|discussion|sig-DDE|[@panchenbo](https://gitee.com/panchenbo)|
|6|[openEuler 21.03 kernel update to version 5.10](https://gitee.com/openeuler/release-management/issues/I27YGU?from=project-issue)|discussion|sig-kernel|[@XieXiuQi](https://gitee.com/xiexiuqi)|
